<Query Kind="Program">
  <Reference>C:\linq\CustomActionsHarness (2)\Phocas.SharedModels.dll</Reference>
  <NuGetReference Version="5.2.3">Microsoft.AspNet.WebApi.Client</NuGetReference>
  <NuGetReference Version="12.0.2">Newtonsoft.Json</NuGetReference>
  <NuGetReference Version="2016.1.0">SSH.NET</NuGetReference>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>Newtonsoft.Json.Linq</Namespace>
  <Namespace>Phocas.SharedModels.CustomActions</Namespace>
  <Namespace>System</Namespace>
  <Namespace>System.Collections.Generic</Namespace>
  <Namespace>System.Data</Namespace>
  <Namespace>System.Data.SqlClient</Namespace>
  <Namespace>System.IO</Namespace>
  <Namespace>System.Linq</Namespace>
  <Namespace>System.Net.Http</Namespace>
  <Namespace>System.Net.Http.Formatting</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

// CA_URL https://sftp.phocaspreview.com/Administration/CustomAction/Index/1
// CA_PARAMETERS .\SFTPParameters.json
// repo test
// CA_REMOVE // CA_ADD, CA_PARAMETERS, CA_REMOVE AND CA_URL are special comments used by the harness. 
// CA_REMOVE // Any lines with CA_ADD will have the '// CA_ADD ' portion removed and the rest of the line will remain.
// CA_REMOVE // A single line with CA_PARAMETERS will be used to determine an (optional) JSON file that contains definitions of parameters for the Custom Action.
// CA_REMOVE // Any lines with CA_REMOVE will be replaced with a blank line (so that line and column numbers are preserved in exception messages).
// CA_REMOVE // CA_URL should be the first line of the LINQPad script and will be used to determine the Phocas organisation and Custom Action ID to interact with.

ICustomActionsAPI API = default(ICustomActionsAPI); // CA_REMOVE
public void Main() { "Looks good.".Dump(); } // CA_REMOVE


// Loaded using the Linqpad Phocas Harness
// This custom action is under source control please see the author beofore making any changes
// Author: Will Ferguson
// Created Date: 2019-11-29 6Pm (c) 2020 Phocas
// See Source Control for changes: https://bitbucket.org/WillFerg/sftp/src/master/SFTP.linq

// CA_ADD Run();
// SFTP
void Run()
{
	// init the variables
	var sourceName = API.Parameter.Get("sourceName");
	var host = API.Parameter.Get("host");
	var username = API.Parameter.Get("username");
	var password = API.Parameter.Get("password");
	var port = Int32.Parse(API.Parameter.Get("port"));
	var passwordAuth = API.SFTP.PasswordAuthentication(username, password);
	var connectionInfo = API.SFTP.ConnectionInfo(host, port, username, passwordAuth);
	var debugMessage = API.Parameter.Get("debugMessage");
	var files = API.TempFilesLocation;
	string[] fileArray = Directory.GetFiles(API.TempFilesLocation);
	
	        OutputMessage(new MyClass { Message = "connection Info"+ connectionInfo.Host + connectionInfo.Username });

	// clean temp files folder
			OutputMessage(new MyClass { Message = "files in Temp: " + fileArray.Count() });
			OutputMessage(new MyClass { Message = "Clean existing temp files in Temp: " + fileArray.Count() });
	foreach (string tempfile in fileArray)
	{
			OutputMessage(new MyClass { Message = "Deleteing Temp file: " + tempfile });
	File.Delete(tempfile);
	}
	
	// Connect to FTP location - Read files into Sync Temp folder
		try
		{
		API.SFTP.Connect(connectionInfo);
		var FTPfiles = API.SFTP.ListFilesInDirectory(".");
		    OutputMessage(new MyClass { Message = "number of FTPfiles found  "+ FTPfiles.Count() });
		API.Parameter.Set("ReadMeLocation", API.TempFilesLocation);
			foreach (var file in FTPfiles)
			{			
			using (Stream fileStream = File.OpenWrite(API.TempFilesLocation + "\\" + file.Name))
				{
					OutputMessage(new MyClass { Message = "Fetching file:"+(file.FullName) });
					API.SFTP.DownloadFile(file.FullName, fileStream);
					OutputMessage(new MyClass { Message = "Download to :"+(API.TempFilesLocation + "\\" + file.FullName) });
				}			
			}
		}
	catch (Exception er)
	{
		API.OutputLine("An exception has been caught " + er.ToString());
	}
	finally
	{
		API.SFTP.Disconnect();
	}

	// Sync files from Temp Folder to Sync Source
	            OutputMessage(new MyClass { Message = "Count of Files in Temp: " + fileArray.Count() });
	            OutputMessage(new MyClass { Message = "Temp file list: " + fileArray });

	foreach (string tempfile in fileArray)
		{			
				OutputMessage(new MyClass { Message ="Processing: " +  tempfile });
	 		var itemData = TextFileToDataTable(tempfile);
	    	var itemName = System.IO.Path.GetFileNameWithoutExtension(tempfile);       
			API.Sync.Sources.Create(sourceName);
			API.Sync.Sources.Start(sourceName);

		try
			{
				OutputMessage(new MyClass { Message ="Sync Items: " +  itemName });
			API.Sync.Items.Create(sourceName, itemName);
			API.Sync.Items.Start(sourceName, itemName);
			API.Sync.Items.ClearData(sourceName, itemName);
			API.Sync.Items.AppendData(sourceName, itemName, itemData);
			API.Sync.Items.End(sourceName, itemName, warningCount: 0);
			}
		catch
			{
			API.Sync.Sources.Log(sourceName, API.Sync.LogSeverities.ERROR, $"Item '{itemName}' failed.");
			}
		}
	API.Sync.Sources.Process(sourceName);
	API.Sync.Sources.End(sourceName);
}

#region MyClass definitions 
void OutputMessage(MyClass myObject)
{
	API.OutputLine(myObject);
}

class MyClass
{
	public string Message { get; set; }
	public override string ToString() => Message;
}
#endregion

#region File to Datatable classes
public static DataTable TextFileToDataTable(string filePath)
{

	// Thanks to https://stackoverflow.com/a/20861183
	var table = new DataTable();
	var fileContents = File.ReadAllLines(filePath);
	var splitFileContents = (from f in fileContents select f.Split(Environment.NewLine.ToCharArray())).ToArray();
	int maxLength = (from s in splitFileContents select s.Count()).Max();

	for (int i = 0; i < maxLength; i++)
		table.Columns.Add();

	foreach (var line in splitFileContents)
	{
		DataRow row = table.NewRow();
		row.ItemArray = (object[])line;
		table.Rows.Add(row);
	}

	return table;
}
#endregion