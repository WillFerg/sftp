<Query Kind="Program">
  <Reference>C:\linq\CustomActionsHarness (2)\Phocas.SharedModels.dll</Reference>
  <NuGetReference Version="5.2.3">Microsoft.AspNet.WebApi.Client</NuGetReference>
  <NuGetReference Version="12.0.2">Newtonsoft.Json</NuGetReference>
  <NuGetReference Version="2016.1.0">SSH.NET</NuGetReference>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>Newtonsoft.Json.Linq</Namespace>
  <Namespace>Phocas.SharedModels.CustomActions</Namespace>
  <Namespace>System</Namespace>
  <Namespace>System.Collections.Generic</Namespace>
  <Namespace>System.Data</Namespace>
  <Namespace>System.Data.SqlClient</Namespace>
  <Namespace>System.IO</Namespace>
  <Namespace>System.Linq</Namespace>
  <Namespace>System.Net.Http</Namespace>
  <Namespace>System.Net.Http.Formatting</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

// CA_URL https://sftp.phocaspreview.com/Administration/CustomAction/Index/4
// CA_PARAMETERS .\SFTPParameters.json

// CA_REMOVE // CA_ADD, CA_PARAMETERS, CA_REMOVE AND CA_URL are special comments used by the harness. 
// CA_REMOVE // Any lines with CA_ADD will have the '// CA_ADD ' portion removed and the rest of the line will remain.
// CA_REMOVE // A single line with CA_PARAMETERS will be used to determine an (optional) JSON file that contains definitions of parameters for the Custom Action.
// CA_REMOVE // Any lines with CA_REMOVE will be replaced with a blank line (so that line and column numbers are preserved in exception messages).
// CA_REMOVE // CA_URL should be the first line of the LINQPad script and will be used to determine the Phocas organisation and Custom Action ID to interact with.

ICustomActionsAPI API = default(ICustomActionsAPI); // CA_REMOVE
public void Main() { "Looks good.".Dump(); } // CA_REMOVE


// Loaded using the Linqpad Phocas Harness
// This custom action is under source control please see the author beofore making any changes
// Author: Will Ferguson
// Date: 2019-11-29  (c) 2020 Phocas
// Purpose: This Custom action loads a single Readme.txt file from PhocasFTP

// CA_ADD Run();



// SFTP
void Run()
{
	// init the variables
	var host = API.Parameter.Get("host");
	var username = API.Parameter.Get("username");
	var password = API.Parameter.Get("password");
	var port = Int32.Parse(API.Parameter.Get("port"));
	var passwordAuth = API.SFTP.PasswordAuthentication(username, password);
	var connectionInfo = API.SFTP.ConnectionInfo(host, port, username, passwordAuth);

	try
	{
		API.SFTP.Connect(connectionInfo);
		var files = API.SFTP.ListFilesInDirectory(".");
		API.Parameter.Set("ReadMeLocation", API.TempFilesLocation);

		foreach (var file in files)
		{
			
			if (file.FullName.Contains("Test.txt"))
			
			{
			    OutputMessage(new MyClass { Message = "Found file on FTP host: " + host + ": "+(files + "\\" + file.FullName) });
				using (Stream fileStream = File.OpenWrite(API.TempFilesLocation + "\\" + file.Name))
				{
					API.SFTP.DownloadFile(file.FullName, fileStream);
				}
			}
		}
	}
	catch (Exception er)
	{
		API.OutputLine("An exception has been caught " + er.ToString());
	}
	finally
	{
		API.SFTP.Disconnect();
	}

	// Sync
	var sourceName = "Simple SFTP (Password Authentication)";
	var itemName = "Test";
	var itemData = TextFileToDataTable(API.TempFilesLocation + "/test.txt");

	API.Sync.Sources.Create(sourceName);
	API.Sync.Sources.Start(sourceName);

	try
	{
		API.Sync.Items.Create(sourceName, itemName);
		API.Sync.Items.Start(sourceName, itemName);

		API.Sync.Items.ClearData(sourceName, itemName);
		API.Sync.Items.AppendData(sourceName, itemName, itemData);

		API.Sync.Items.End(sourceName, itemName, warningCount: 0);
	}
	catch
	{
		API.Sync.Sources.Log(sourceName, Sync.LogSeverities.ERROR, $"Item '{itemName}' failed.");
	}

	API.Sync.Sources.Process(sourceName);
	API.Sync.Sources.End(sourceName);
}

void OutputMessage(MyClass myObject)
{
	API.OutputLine(myObject);
}

class MyClass
{
	public string Message { get; set; }

	public override string ToString() => Message;
}

public static DataTable TextFileToDataTable(string filePath)
{
	// Thanks to https://stackoverflow.com/a/20861183
	var table = new DataTable();
	var fileContents = File.ReadAllLines(filePath);
	var splitFileContents = (from f in fileContents select f.Split(Environment.NewLine.ToCharArray())).ToArray();
	int maxLength = (from s in splitFileContents select s.Count()).Max();

	for (int i = 0; i < maxLength; i++)
		table.Columns.Add();

	foreach (var line in splitFileContents)
	{
		DataRow row = table.NewRow();
		row.ItemArray = (object[])line;
		table.Rows.Add(row);
	}

	return table;
}
